package com.demo.salesforce.api.soap;

public class SalesforceStaticAPIData {
	
	public static final String COMPETITOR_ACCOUNT_RECORDTYPEID = "012j00000010Ci6AAE";
	public static final String CONSULTING_FIRM_ACCOUNT_RECORDTYPEID = "012j00000010Ci7AAE";
	public static final String EMPLOYER_ACCOUNT_RECORDTYPEID = "012j00000010MEzAAM";
	public static final String INACTIVE_ACCOUNT_RECORDTYPEID = "012j00000015D00AAE";
	public static final String SPONSOR_ACCOUNT_RECORDTYPEID = "012j00000010Ci8AAE";
	
	public static final String CONTACT_CONTACT_RECORDTYPEID = "012f10000010MXjAAM";
	public static final String PARTICIPANT_CONTACT_RECORDTYPEID = "012f10000010MXkAAM";
	public static final String PROSPECTIVE_PARTICIPANT_CONTACT_RECORDTYPEID = "012f1000000uj9sAAA";

}
