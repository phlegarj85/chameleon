package com.demo.windows;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.windows.ExtendedWindowsDriver;

public class BaseUIPage {

    protected ExtendedWindowsDriver getDriver() {
        return DriverManager.getWindowsDriver();
    }
}
