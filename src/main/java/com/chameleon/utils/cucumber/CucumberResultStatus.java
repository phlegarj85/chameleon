package com.chameleon.utils.cucumber;

import org.apache.commons.lang.StringUtils;

import com.chameleon.AutomationException;

public enum CucumberResultStatus {
    PASSED("passed"),
    FAILED("failed"),
    SKIPPED("skipped");

    private final String status;

    CucumberResultStatus(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static CucumberResultStatus fromString(String status) {
        for (CucumberResultStatus s : values()) {
            if (s.toString().equalsIgnoreCase(status)) {
                return s;
            }
        }
        throw new AutomationException("No Status defined found for requested value [ " + status + " ]");
    }

    @Override
    public String toString() {
        return StringUtils.capitalize(status);
    }
}
