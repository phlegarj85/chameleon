package com.chameleon.utils.cucumber.objects;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Element {

    @JsonProperty("before")
    private List<Before> before = null;
    @JsonProperty("line")
    private Long line;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("id")
    private String id;
    @JsonProperty("after")
    private List<After> after = null;
    @JsonProperty("type")
    private String type;
    @JsonProperty("keyword")
    private String keyword;
    @JsonProperty("steps")
    private List<Step> steps = null;
    @JsonProperty("tags")
    private List<Tag> tags = null;

    @JsonProperty("before")
    public List<Before> getBefore() {
        if (CollectionUtils.isEmpty(before)) {
            before = new ArrayList<>();
        }
        return before;
    }

    @JsonProperty("before")
    public void setBefore(List<Before> before) {
        this.before = before;
    }

    @JsonProperty("line")
    public Long getLine() {
        return line;
    }

    @JsonProperty("line")
    public void setLine(Long line) {
        this.line = line;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("after")
    public List<After> getAfter() {
        if (CollectionUtils.isEmpty(after)) {
            after = new ArrayList<>();
        }
        return after;
    }

    @JsonProperty("after")
    public void setAfter(List<After> after) {
        this.after = after;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("keyword")
    public String getKeyword() {
        return keyword;
    }

    @JsonProperty("keyword")
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @JsonProperty("steps")
    public List<Step> getSteps() {
        if (CollectionUtils.isEmpty(steps)) {
            steps = new ArrayList<>();
        }
        return steps;
    }

    @JsonProperty("steps")
    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    @JsonProperty("tags")
    public List<Tag> getTags() {
        if (CollectionUtils.isEmpty(tags)) {
            tags = new ArrayList<>();
        }
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

}