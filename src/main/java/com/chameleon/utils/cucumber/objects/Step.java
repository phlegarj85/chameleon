package com.chameleon.utils.cucumber.objects;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Step {

    @JsonProperty("result")
    private Result result;
    @JsonProperty("line")
    private Long line;
    @JsonProperty("name")
    private String name;
    @JsonProperty("match")
    private Match match;
    @JsonProperty("keyword")
    private String keyword;
    @JsonProperty("output")
    private List<String> output = null;

    @JsonProperty("result")
    public Result getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(Result result) {
        this.result = result;
    }

    @JsonProperty("line")
    public Long getLine() {
        return line;
    }

    @JsonProperty("line")
    public void setLine(Long line) {
        this.line = line;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("match")
    public Match getMatch() {
        return match;
    }

    @JsonProperty("match")
    public void setMatch(Match match) {
        this.match = match;
    }

    @JsonProperty("keyword")
    public String getKeyword() {
        return keyword;
    }

    @JsonProperty("keyword")
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @JsonProperty("output")
    public List<String> getOutput() {
        if (CollectionUtils.isEmpty(output)) {
            output = new ArrayList<>();
        }
        return output;
    }

    @JsonProperty("output")
    public void setOutput(List<String> output) {
        this.output = output;
    }

}