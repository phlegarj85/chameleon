package com.chameleon.utils.cucumber;

import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_BREAKDOWN_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_COMMON_FAILING_REASONS_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_LONGEST_DURATION_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_REPORT_EXECUTION_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_REPORT_GENERATE_PDF;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_REPORT_NAME;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_REPORT_TEST_DETAILS_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_REPORT_TIMESTAMP_FORMAT;
import static com.chameleon.utils.cucumber.CucumberConstants.DEFAULT_REPORT_TIMEZONE;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_BREAKDOWN_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_COMMON_FAILING_REASONS_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_EXECUTION_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_LONGEST_DURATION_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_REPORT_NAME;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_TEST_DETAILS_SUAMMRY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_TIMESTAMP_FORMAT;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_TIMESTAMP_ZONE;
import static com.chameleon.utils.cucumber.CucumberConstants.REPORT_OUTPUT_LOCATION;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.chameleon.AutomationException;
import com.chameleon.utils.Randomness;
import com.chameleon.utils.cucumber.objects.Element;
import com.chameleon.utils.cucumber.objects.FeatureReport;
import com.chameleon.utils.cucumber.objects.Result;
import com.chameleon.utils.cucumber.objects.Step;
import com.chameleon.utils.cucumber.objects.Tag;
import com.chameleon.utils.date.DateTimeConversion;
import com.chameleon.utils.io.FileLoader;
import com.chameleon.utils.io.JsonObjectMapper;
import com.chameleon.utils.io.PomProperties;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class CucumberMavenReport {
    private static final String TIMESTAMP = PomProperties.getProperty(POM_CUCUMBER_OPTION_TIMESTAMP_FORMAT) == null
            ? DEFAULT_REPORT_TIMESTAMP_FORMAT
            : PomProperties.getProperty(POM_CUCUMBER_OPTION_TIMESTAMP_FORMAT);

    private static final ZoneId TIMEZONE = PomProperties.getProperty(POM_CUCUMBER_OPTION_TIMESTAMP_ZONE) == null
            ? ZoneId.of(DEFAULT_REPORT_TIMEZONE)
            : ZoneId.of(PomProperties.getProperty(POM_CUCUMBER_OPTION_TIMESTAMP_ZONE));

    private static final String PASSED = "Passed";
    private static final String FAILED = "Failed";
    private static final String SKIPPED = "Skipped";
    private static final String TEST_DETAILS_NAME = "testDetailsName";
    private static final String SCENARIO_DETAILS_NAME = "scenarioDetailsName";
    private static final String DETAILS_NAME = "detailsName";
    private static final String DETAILS_STATUS = "detailsStatus";
    private static final String DETAILS_DURATION = "detailsDuration";
    private static final String DETAILS_USTC = "detailsUstc";

    private static final String BREAK = "<br/>";
    private static final String HEADER = "<h3>%s</h3>";
    private static final String START_TABLE = "<table class='table table-hover'>";
    private static final String END_TABLE = "</table>";
    private static final String START_ROW = "<tr class='table table-hover'>";
    private static final String END_ROW = "</tr>";
    private static final String ADD_CELL_HEADER = "<th class='%s'>%s</th>";
    private static final String ADD_CELL_DATA = "<td class='%s'>%s</td>";
    private static final String ADD_SCREENSHOT = "<td class='%s'><a target='_blank' href='%s'> <img src='file:///%s' height='200' width='300'/> </a></td>";
    private static StringBuilder buffer = null;

    public static void main(String[] args) {
        final String startTime = getExecutionStartTime();
        final String endTime = Randomness.generateCurrentDatetime(TIMESTAMP);

        // Get data from JSON files, add HTML details to details collection
        final List<String> passedDetails = new ArrayList<>();
        final List<String> failedDetails = new ArrayList<>();
        final CucumberResults results = new CucumberResults();

        readResults(results, passedDetails, failedDetails);

        final String executionSummaryEnabled = PomProperties.getProperty(POM_CUCUMBER_OPTION_EXECUTION_SUMMARY_ENABLED);
        final String executionBreakdownSummaryEnabled = PomProperties.getProperty(POM_CUCUMBER_OPTION_BREAKDOWN_SUMMARY_ENABLED);
        final String longestDurationSummaryEnabled = PomProperties.getProperty(POM_CUCUMBER_OPTION_LONGEST_DURATION_SUMMARY_ENABLED);
        final String commonFailingSummaryEnabled = PomProperties.getProperty(POM_CUCUMBER_OPTION_COMMON_FAILING_REASONS_SUMMARY_ENABLED);
        final String testDetailsSummaryEnabled = PomProperties.getProperty(POM_CUCUMBER_OPTION_TEST_DETAILS_SUAMMRY_ENABLED);

        // Start report buffer
        buffer = new StringBuilder();
        if (executionSummaryEnabled == null ? DEFAULT_REPORT_EXECUTION_SUMMARY_ENABLED : Boolean.parseBoolean(executionSummaryEnabled)) {
            buildExecutionSummaryTable(results.getPassRate(), startTime, endTime);
        }

        if (executionBreakdownSummaryEnabled == null ? DEFAULT_BREAKDOWN_SUMMARY_ENABLED : Boolean.parseBoolean(executionBreakdownSummaryEnabled)) {
            buildExecutionBreakdownSummaryTable(results);
        }

        if (longestDurationSummaryEnabled == null ? DEFAULT_LONGEST_DURATION_SUMMARY_ENABLED : Boolean.parseBoolean(longestDurationSummaryEnabled)) {
            buildLongestDurationSteps(results);
        }

        if (commonFailingSummaryEnabled == null ? DEFAULT_COMMON_FAILING_REASONS_SUMMARY_ENABLED : Boolean.parseBoolean(commonFailingSummaryEnabled)) {
            buildCommonFailingReasons(results);
        }

        if (testDetailsSummaryEnabled == null ? DEFAULT_REPORT_TEST_DETAILS_SUMMARY_ENABLED : Boolean.parseBoolean(testDetailsSummaryEnabled)) {
            buildTestDetailsTable(passedDetails, failedDetails);
        }

        writeOutReport();

    }

    private static String getExecutionStartTime() {
        // retrieve the start time of the test execution which was saved to the disk by Jenkins
        final String executionStartTimestampFilename = PomProperties.getProperty("execution.start.timestamp");

        if (StringUtils.isNotEmpty(executionStartTimestampFilename)) {
            final SimpleDateFormat executionStartTimestampSdf = new SimpleDateFormat(TIMESTAMP);

            final String executionStartTimestampString = FileLoader.readFile(executionStartTimestampFilename);
            Date executionStartTime = null;
            try {
                executionStartTime = executionStartTimestampSdf.parse(executionStartTimestampString);
            } catch (ParseException e) {
                throw new AutomationException("Failed to parse date from timestamp file");
            }

            final long current = Instant.now().atZone(TIMEZONE).toEpochSecond();
            final long start = executionStartTime.toInstant().atZone(TIMEZONE).toEpochSecond();
            final long duration = current - start;
            final long hours = duration / 3600;
            final long minutes = (duration % 3600) / 60;
            final long seconds = duration % 60;

            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }
        return "N/A";
    }

    private static void readResults(final CucumberResults results, final List<String> passedDetails, final List<String> failedDetails) {

        File directory = new File(CucumberConstants.RUNNER_RESULTS_LOCATION);
        // get all the files from a directory
        File[] fList = directory.listFiles();

        for (File file : fList) {
            final List<String> passedScenarioDetails = new ArrayList<>();
            final List<String> failedScenarioDetails = new ArrayList<>();
            if (!file.isFile()) {
                continue;
            }

            boolean currentFeatureIsSkipped = false;
            boolean currentFeatureIsFailed = false;
            String currentFeatureStatus;
            double currentFeatureDuration = 0;
            final String curPath = file.getAbsolutePath();
            final FeatureReport feature = JsonObjectMapper.readJsonFromFile(curPath, FeatureReport[].class)[0];

            // Get feature name
            final String featureName = feature.getName();

            // Get Tags
            final String currentFeatureID = getTagName(feature.getTags(), CucumberConstants.FEATURE_TAG);

            // Get Scenarios
            List<Element> scenarios = feature.getElements();

            for (Element scenario : scenarios) {
                String screenShotPath = null;
                boolean currentScenarioIsSkipped = false;
                boolean currentScenarioIsFailed = false;

                String currentScenarioStatus;
                long currentScenarioDuration = 0L;

                List<String> stepDetails = new ArrayList<>();

                // Get scenario name
                String scenarioName = scenario.getName();

                // Get Tags

                final String currentScenarioID = getTagName(feature.getTags(), CucumberConstants.SCENARIO_TAG);

                String errorReason = null;
                // Get steps and step results
                List<Step> steps = scenario.getSteps();
                for (Step step : steps) {
                    final String stepName = step.getName();
                    final Result stepresults = step.getResult();
                    final String stepStatus = stepresults.getStatus();
                    long duration = 0;
                    String currentStepStatus = null;

                    // Get screenshot path if one exists.
                    if (!step.getOutput().isEmpty()) {
                        final List<String> outputs = step.getOutput();
                        screenShotPath = outputs.get(0);

                    }

                    results.incrementTotalStepCount();
                    if (stepStatus.equalsIgnoreCase(PASSED)) {
                        results.incrementStepPassCount();
                        duration = stepresults.getDuration();
                        currentFeatureIsSkipped = false;
                        currentScenarioIsSkipped = false;
                        currentStepStatus = PASSED;
                        errorReason = null;
                    } else if (stepStatus.equalsIgnoreCase(SKIPPED)) {
                        results.incrementStepSkipCount();
                        currentStepStatus = SKIPPED;
                        currentFeatureIsSkipped = true;
                        currentScenarioIsSkipped = true;
                        errorReason = null;
                    } else {
                        currentStepStatus = FAILED;
                        currentFeatureIsFailed = true;
                        currentScenarioIsFailed = true;
                        duration = stepresults.getDuration();
                        results.incrementStepFailCount();
                        errorReason = results.addException(step.getResult().getErrorMessage());

                    }

                    final long stepSeconds = duration / 1000000000;
                    final String stepDuration = DateTimeConversion.millisToHourMinuteSecond(stepSeconds * 1000);
                    results.addStepDuration(currentFeatureID + ": " + stepName, stepSeconds);
                    currentFeatureDuration = currentFeatureDuration + stepSeconds;
                    currentScenarioDuration = currentScenarioDuration + stepSeconds;

                    final String reportedStep = StringUtils.isEmpty(errorReason) ? stepName : stepName + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Failure Reason</b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + errorReason;

                    addTestStepDetailRow(stepDetails, reportedStep, currentStepStatus, stepDuration, currentScenarioID);
                }

                results.incrementTotalScenarioCount();
                if (currentScenarioIsFailed) {
                    results.incrementScenarioFailCount();
                    currentScenarioStatus = FAILED;
                } else if (currentScenarioIsSkipped) {
                    results.incrementScenarioSkipCount();
                    currentScenarioStatus = SKIPPED;
                } else {
                    results.incrementScenarioPassCount();
                    currentScenarioStatus = PASSED;
                }

                final String scenarioDuration = DateTimeConversion.millisToHourMinuteSecond(currentScenarioDuration * 1000);
                // new CSS Formatted scenario details
                if (currentFeatureIsSkipped || currentScenarioIsFailed) {
                    addTestScenarioDetailRow(failedScenarioDetails, "<b>" + "Scenario: " + scenarioName, currentScenarioStatus, scenarioDuration, currentScenarioID, screenShotPath);

                    // Adding steps to html report ONLY if scenario has failed or skipped.
                    failedScenarioDetails.addAll(stepDetails);
                } else {
                    addTestScenarioDetailRow(passedScenarioDetails, "<b>" + "Scenario: " + scenarioName + "</b>", currentScenarioStatus, scenarioDuration, currentScenarioID, null);
                }
            }

            results.incrementTotalFeatureCount();
            if (currentFeatureIsFailed) {
                results.incrementFeatureFailCount();
                currentFeatureStatus = FAILED;
            } else if (currentFeatureIsSkipped) {
                results.incrementFeatureSkipCount();
                currentFeatureStatus = SKIPPED;
            } else {
                results.incrementFeaturePassCount();
                currentFeatureStatus = PASSED;
            }

            final String featureDuration = DateTimeConversion.millisToHourMinuteSecond((long) (currentFeatureDuration * 1000));
            // new CSS Formatted scenario details
            if (currentFeatureIsFailed || currentFeatureIsSkipped) {
                addTestDetailRow(failedDetails, "<b>" + "Feature: " + featureName + "</b>", currentFeatureStatus, featureDuration, "<b>" + currentFeatureID + "</b>");
                // add scenarios and any skipped or failed steps to the report table.
                failedDetails.addAll(failedScenarioDetails);
            } else {
                addTestDetailRow(passedDetails, "<b>" + "Feature: " + featureName + "</b>", currentFeatureStatus, featureDuration, "<b>" + currentFeatureID + "</b>");
                passedDetails.addAll(passedScenarioDetails);
            }
        }

    }

    private static void addTestDetailRow(final List<String> details, final String name, final String status, final String duration, final String feature) {
        final String capStatus = StringUtils.capitalize(status);
        details.add(START_ROW);
        details.add(String.format(ADD_CELL_DATA, TEST_DETAILS_NAME + status, name));
        details.add(String.format(ADD_CELL_DATA, DETAILS_STATUS + status, capStatus));
        details.add(String.format(ADD_CELL_DATA, DETAILS_DURATION + status, duration));
        details.add(String.format(ADD_CELL_DATA, DETAILS_USTC + status, feature));
        details.add(END_ROW);
    }

    private static void addTestStepDetailRow(final List<String> details, final String name, final String status, final String duration, final String feature) {
        final String capStatus = StringUtils.capitalize(status);
        details.add(START_ROW);
        details.add(String.format(ADD_CELL_DATA, DETAILS_NAME + status, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + name));
        details.add(String.format(ADD_CELL_DATA, DETAILS_STATUS + status, capStatus));
        details.add(String.format(ADD_CELL_DATA, DETAILS_DURATION + status, duration));
        details.add(String.format(ADD_CELL_DATA, DETAILS_USTC + status, feature));
        details.add(END_ROW);
    }

    private static void addTestScenarioDetailRow(final List<String> details, final String name, final String status, final String duration, final String feature, final String screenShotPath) {
        final String capStatus = StringUtils.capitalize(status);
        details.add(START_ROW);
        details.add(String.format(ADD_CELL_DATA, SCENARIO_DETAILS_NAME + status, "&nbsp;&nbsp;&nbsp;" + name));
        details.add(String.format(ADD_CELL_DATA, DETAILS_STATUS + status, capStatus));
        details.add(String.format(ADD_CELL_DATA, DETAILS_DURATION + status, duration));
        details.add(String.format(ADD_CELL_DATA, DETAILS_USTC + status, feature));
        if (screenShotPath != null) {
            details.add(String.format(ADD_SCREENSHOT, "detailsScenarioStatusFailed", screenShotPath, screenShotPath));
        }
        details.add(END_ROW);
    }

    private static void writeOutReport() {

        final String reportName = PomProperties.getProperty(POM_CUCUMBER_OPTION_REPORT_NAME) == null
                ? DEFAULT_REPORT_NAME
                : PomProperties.getProperty(POM_CUCUMBER_OPTION_REPORT_NAME);

        final SimpleDateFormat sdfWithTimeZoneName = new SimpleDateFormat(TIMESTAMP);
        final Date now = new Date();
        final String strDate = sdfWithTimeZoneName.format(now);
        final String fileName = reportName.replace(" ", "_") + "_Results-" + strDate.replaceAll(" ", "_");
        final String currentReportFileHTML = REPORT_OUTPUT_LOCATION + fileName + ".html";
        final String currentReportFilePDF = REPORT_OUTPUT_LOCATION + fileName + ".pdf";

        // ensure cucumberReport directory exists
        File directory = new File(REPORT_OUTPUT_LOCATION);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        String report = FileLoader.readFile(CucumberConstants.REPORT_TEMPLATE_LOCATION);
        report = report.replace("<!-- REPORT REPLACE -->", buffer.toString());
        report = report.replace("<!-- REPORT NAME REPLACE -->", reportName);
        writeOutHTML(report, currentReportFileHTML);
        buffer.setLength(0);
        buffer = null;

        final String generatePdf = PomProperties.getProperty(REPORT_OUTPUT_LOCATION);
        if (generatePdf == null ? DEFAULT_REPORT_GENERATE_PDF : Boolean.parseBoolean(generatePdf)) {
            Document document = new Document();
            PdfWriter writer;
            try {
                writer = PdfWriter.getInstance(document,
                        new FileOutputStream(currentReportFilePDF));
                document.open();
                XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                        new FileInputStream(currentReportFileHTML));
                document.close();
            } catch (IOException | DocumentException e) {
                throw new AutomationException("Failed to generate PDF file", e);
            }

            // inject link to PDF
            report = FileLoader.readFile(currentReportFileHTML);
            writeOutHTML(report, currentReportFileHTML);
        }
    }

    private static void writeOutHTML(final String buffer, final String file) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            final int aLength = buffer.length();
            final int aChunk = 1024;// 1 kb buffer to read data from
            final char[] aChars = new char[aChunk];

            for (int aPosStart = 0; aPosStart < aLength; aPosStart += aChunk) {
                final int aPosEnd = Math.min(aPosStart + aChunk, aLength);
                buffer.getChars(aPosStart, aPosEnd, aChars, 0); // Create no new buffer
                bw.write(aChars, 0, aPosEnd - aPosStart);// This is faster than just copying one byte at the time
            }
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();// NOSONAR
        }
    }

    private static void buildExecutionSummaryTable(final String passRate, final String duration, final String executionTime) {
        appendLine(HEADER, "Test Execution Summary");
        appendLine(START_TABLE);
        buildExecutionSummaryRow("Environment", PomProperties.getProperty("test.environment"));
        buildExecutionSummaryRow("Run Location", PomProperties.getProperty("test.run.location"));
        buildExecutionSummaryRow("Build Tag", PomProperties.getProperty("cucumber.tags"));
        buildExecutionSummaryRow("Build Number", PomProperties.getProperty("build.num"));
        buildExecutionSummaryRow("Test Pass Rate", passRate + "%");
        buildExecutionSummaryRow("Duration", duration);
        buildExecutionSummaryRow("Execution Time", executionTime);
        appendLine(END_TABLE);
        appendLine(BREAK);
        appendLine(BREAK);
    }

    private static void buildExecutionSummaryRow(final String name, final String value) {
        appendLine(START_ROW);
        appendLine(ADD_CELL_HEADER, "summaryMetricNameHeader", name);
        appendLine(ADD_CELL_DATA, "summaryMetricValueData", value);
        appendLine(END_ROW);
    }

    private static void buildExecutionBreakdownSummaryRow(final String catagoryName, final int featureCount, int scenarioCount, int stepCount) {
        appendLine(START_ROW);
        appendLine(ADD_CELL_HEADER, "executionBreakdownMetricNameHeader", catagoryName);
        appendLine(ADD_CELL_DATA, "executionBreakdownFeaturesValueData", featureCount);
        appendLine(ADD_CELL_DATA, "executionBreakdownScenariosValueData", scenarioCount);
        appendLine(ADD_CELL_DATA, "executionBreakdownStepsValueData", stepCount);
        appendLine(END_ROW);
    }

    private static void buildExecutionBreakdownSummaryTable(final CucumberResults results) {
        appendLine(HEADER, "Test Execution Breakdown");
        appendLine(START_TABLE);
        appendLine(START_ROW);
        appendLine(ADD_CELL_HEADER, "executionBreakdownMetricNameHeader", "Category");
        appendLine(ADD_CELL_HEADER, "executionBreakdownFeaturesValueData", "Features");
        appendLine(ADD_CELL_HEADER, "executionBreakdownFeaturesValueData", "Scenarios");
        appendLine(ADD_CELL_HEADER, "executionBreakdownFeaturesValueData", "Steps");
        appendLine(END_ROW);

        buildExecutionBreakdownSummaryRow("Total", results.getTotalFeatureCount(), results.getTotalScenarioCount(), results.getTotalStepCount());
        buildExecutionBreakdownSummaryRow(PASSED, results.getFeaturePassCount(), results.getScenarioPassCount(), results.getStepPassCount());
        buildExecutionBreakdownSummaryRow(SKIPPED, results.getFeatureFailCount(), results.getScenarioSkipCount(), results.getStepSkipCount());
        buildExecutionBreakdownSummaryRow(FAILED, results.getFeatureFailCount(), results.getScenarioFailCount(), results.getStepFailCount());

        appendLine(END_TABLE);
        appendLine(BREAK);
        appendLine(BREAK);

    }

    private static void buildLongestDurationSteps(final CucumberResults results) {
        appendLine(HEADER, "Top 10 Longest Duration Steps");
        appendLine(START_TABLE);
        appendLine(START_ROW);
        appendLine(ADD_CELL_HEADER, "longestStepHeader", "Step Name");
        appendLine(ADD_CELL_HEADER, "longestStepDurationHeader", "Duration");
        appendLine(END_ROW);

        for (Entry<String, Long> step : results.getStepDurations(10).entrySet()) {
            appendLine(START_ROW);
            appendLine(ADD_CELL_DATA, "longestStep", step.getKey());
            appendLine(ADD_CELL_DATA, "longestStepDuration", DateTimeConversion.millisToHourMinuteSecond(step.getValue() * 1000));
            appendLine(END_ROW);
        }
        appendLine(END_TABLE);
        appendLine(BREAK);
        appendLine(BREAK);

    }

    private static void buildCommonFailingReasons(final CucumberResults results) {
        appendLine(HEADER, "Top 10 Failing Reasons");
        appendLine(START_TABLE);
        appendLine(START_ROW);
        appendLine(ADD_CELL_HEADER, "longestStepHeader", "Exception");
        appendLine(ADD_CELL_HEADER, "longestStepDurationHeader", "Times occurred");
        appendLine(END_ROW);

        for (Entry<String, Integer> exception : results.getExceptions(10).entrySet()) {
            appendLine(START_ROW);
            appendLine(ADD_CELL_DATA, "longestStep", exception.getKey());
            appendLine(ADD_CELL_DATA, "longestStepDuration", exception.getValue());
            appendLine(END_ROW);
        }
        appendLine(END_TABLE);
        appendLine(BREAK);
        appendLine(BREAK);

    }

    private static void buildTestDetailsTable(final List<String> passedDetails, final List<String> failedDetails) {
        appendLine(HEADER, "Test Details");
        appendLine(START_TABLE);
        appendLine(START_ROW);
        appendLine(ADD_CELL_HEADER, "detailsNameHeader", "Name");
        appendLine(ADD_CELL_HEADER, "detailsStatusHeader", "Status");
        appendLine(ADD_CELL_HEADER, "detailsDurationHeader", "Duration");
        appendLine(ADD_CELL_HEADER, "detailsUstcHeader", "User Story<br/>Test Case");
        appendLine(ADD_CELL_HEADER, "detailsUstcHeader", "Screenshot");
        appendLine(END_ROW);

        appendLines(failedDetails);
        appendLines(passedDetails);

        appendLine(END_TABLE);
        appendLine(BREAK);
        appendLine(BREAK);
    }

    private static void appendLine(final String line, final Object... options) {
        buffer.append(String.format(line, options));
    }

    private static void appendLines(final List<String> lines) {
        lines.forEach(line -> buffer.append(line));
    }

    private static String getTagName(final List<Tag> tags, final String tagReference) {
        for (Tag tag : tags) {
            // Get tag name
            final String name = tag.getName();
            if (name.toUpperCase().contains("@" + tagReference)) {
                return name.replaceAll("(?i)[@a-z]+", tagReference);
            }

        }
        return null;
    }
}
