package com.chameleon.utils.io;

import static com.chameleon.utils.TestReporter.logTrace;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.text.PDFTextStripper;

import com.chameleon.AutomationException;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.exception.PDFValidationException;

public class PDFUtil {
    private String imageDestinationPath;
    private Color color;
    private PDFTextStripper stripper;
    private CompareMode compareMode;
    private String[] excludePattern;
    private int startPage = 1;
    private int endPage = -1;

    public enum CompareMode {
        TEXT_MODE,
        VISUAL_MODE
    }

    public PDFUtil() {
        this.color = Color.MAGENTA;
        this.compareMode = CompareMode.TEXT_MODE;
        System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
    }

    public void setCompareMode(final CompareMode mode) {
        this.compareMode = mode;
    }

    public CompareMode getCompareMode() {
        return this.compareMode;
    }

    public void excludeText(final String... regexs) {
        this.excludePattern = regexs;
    }

    public String getImageDestinationPath() {
        return this.imageDestinationPath;
    }

    public void setImageDestinationPath(final String path) {
        this.imageDestinationPath = path;
    }

    public void useStripper(final PDFTextStripper stripper) {
        this.stripper = stripper;
    }

    public int getPageCount(final String file) {
        int pageCount = 0;
        try (PDDocument doc = openDoc(file)) {
            return pageCount;
        } catch (IOException e) {
            throw new AutomationException("Failed to get page count", e);
        }
    }

    public String getText(final String file) {
        return this.getPdfText(file, -1, -1);
    }

    public String getText(final String file, final int startPage) {
        return this.getPdfText(file, startPage, -1);
    }

    public String getText(final String file, final int startPage, final int endPage) {
        return this.getPdfText(file, startPage, endPage);
    }

    public boolean compare(final String file1, final String file2) {
        return this.comparePdfFiles(file1, file2, -1, -1);
    }

    public boolean compare(final String file1, final String file2, final int startPage, final int endPage) {
        return this.comparePdfFiles(file1, file2, startPage, endPage);
    }

    public boolean compare(final String file1, final String file2, final int startPage) {
        return this.comparePdfFiles(file1, file2, startPage, -1);
    }

    private boolean comparePdfFiles(final String file1, final String file2, final int startPage, final int endPage) {
        logTrace("Entering PDFUtil#comparePdfFiles");
        boolean result = true;

        logTrace("Will compare PDF's in [ " + this.compareMode.name() + " ]");
        if (CompareMode.TEXT_MODE == this.compareMode) {
            result = comparePdfFilesWithTextMode(file1, file2, startPage, endPage);
        } else {
            result = comparePdfByImage(file1, file2, startPage, endPage);
        }
        logTrace("Exiting PDFUtil#comparePdfFiles");
        return result;
    }

    private boolean comparePdfFilesWithTextMode(final String file1, final String file2, final int startPage, final int endPage) {
        logTrace("Entering PDFUtil#comparePdfFilesWithTextMode");
        logTrace("Retrieve PDF 1 text");
        String file1Txt = this.getPdfText(file1, startPage, endPage).trim();

        logTrace("Retrieve PDF 2 text");
        String file2Txt = this.getPdfText(file2, startPage, endPage).trim();

        if (null != this.excludePattern && this.excludePattern.length > 0) {
            logTrace("Pattern exclusion requested. Excluding following [ " + this.excludePattern + " ]");
            for (int i = 0; i < this.excludePattern.length; i++) {
                file1Txt = file1Txt.replaceAll(this.excludePattern[i], "");
                file2Txt = file2Txt.replaceAll(this.excludePattern[i], "");
            }
        }

        logTrace("Compare text of PDF file");
        boolean result = file1Txt.equalsIgnoreCase(file2Txt);

        logTrace("PDF compare result is [ " + result + " ]");
        logTrace("Exiting PDFUtil#comparePdfFilesWithTextMode");
        return result;
    }

    private boolean comparePdfByImage(final String file1, final String file2, final int startPage, final int endPage) {
        logTrace("Entering PDFUtil#comparePdfByImage");
        logTrace("Ensure page counts between PDFs are the same");
        int pgCount1 = this.getPageCount(file1);
        int pgCount2 = this.getPageCount(file2);

        if (pgCount1 != pgCount2) {
            logTrace("Page count different between files. PDF 1 has [ " + pgCount1 + " ] pages and PDF 2 has [ " + pgCount2 + " ] pages ");
            TestReporter.logFailure("Page count different between files. PDF 1 has [ " + pgCount1 + " ] pages and PDF 2 has [ " + pgCount2 + " ] pages ");
            return false;
        }

        this.updateStartAndEndPages(file1, startPage, endPage);

        logTrace("Start image comparision");
        boolean result = this.convertToImageAndCompare(file1, file2, this.startPage, this.endPage);
        logTrace("Exiting PDFUtil#comparePdfByImage");
        return result;
    }

    private boolean convertToImageAndCompare(final String file1, final String file2, final int startPage, final int endPage) {
        logTrace("Entering PDFUtil#convertToImageAndCompare");
        boolean result = true;

        PDFRenderer pdfRenderer1 = null;
        PDFRenderer pdfRenderer2 = null;

        logTrace("Open both PDF files");
        try (PDDocument doc1 = openDoc(file1); PDDocument doc2 = openDoc(file2)) {

            logTrace("Create a renderer for PDF to extract image");
            pdfRenderer1 = new PDFRenderer(doc1);
            pdfRenderer2 = new PDFRenderer(doc2);

            for (int iPage = startPage - 1; iPage < endPage; iPage++) {
                String fileName = new File(file1).getName().replace(".pdf", "_") + (iPage + 1);
                fileName = fileName + "_diff";

                logTrace("Generate images for page [ " + (iPage + 1) + " ]");
                BufferedImage image1 = pdfRenderer1.renderImageWithDPI(iPage, 300, ImageType.RGB);
                BufferedImage image2 = pdfRenderer2.renderImageWithDPI(iPage, 300, ImageType.RGB);

                logTrace("Compare images for page [ " + (iPage + 1) + " ]");
                result = ImageUtil.compareAndHighlight(image1, image2, fileName, this.color) && result;
            }

            logTrace("Finished with PDF visual compare with result of [ " + result + " ]");
        } catch (Exception e) {
            logTrace("Failed to parse PDF. " + e.getMessage());
            throw new PDFValidationException("Failed to parse PDF", e);
        }

        logTrace("Exiting PDFUtil#convertToImageAndCompare");
        return result;
    }

    private String getPdfText(final String file, final int startPage, final int endPage) {
        String txt = null;
        logTrace("Entering PDFUtil#getPdfText");
        logTrace("Open the PDF file");
        try (PDDocument doc = openDoc(file)) {

            logTrace("Generate text stripper if one has not been provided");
            PDFTextStripper localStripper = new PDFTextStripper();
            if (null != this.stripper) {
                localStripper = this.stripper;
            }

            this.updateStartAndEndPages(file, startPage, endPage);
            localStripper.setStartPage(this.startPage);
            localStripper.setEndPage(this.endPage);

            logTrace("Retrieve text between pages [ " + this.startPage + " ] and [ " + this.endPage + " ]");
            txt = localStripper.getText(doc).trim();
        } catch (IOException e) {
            throw new PDFValidationException("Failed PDF File", e);
        }

        logTrace("Exiting PDFUtil#getPdfText");
        return txt;
    }

    private PDDocument openDoc(final String fileName) {
        logTrace("Entering PDFUtil#openDoc");
        PDDocument doc = null;
        try {
            logTrace("Determine absolute filepath for file [ " + fileName + " ]");
            URL url = FileLoader.class.getResource(fileName);

            logTrace("Attempt to open PDF on absolute filepath [ " + url.getPath() + " ]");
            doc = PDDocument.load(new File(url.getPath()));
        } catch (IOException e) {
            throw new PDFValidationException("Failed to open PDF File", e);
        }

        logTrace("Exiting PDFUtil#openDoc");
        return doc;
    }

    private void updateStartAndEndPages(String file, int start, int end) {
        logTrace("Entering PDFUtil#updateStartAndEndPages");
        try (PDDocument document = openDoc(file)) {
            int pagecount = document.getNumberOfPages();

            if ((start > 0 && start <= pagecount)) {
                this.startPage = start;
            } else {
                this.startPage = 1;
            }

            logTrace("Setting starting page as [ " + this.startPage + " ]");
            if ((end > 0 && end >= start && end <= pagecount)) {
                this.endPage = end;
            } else {
                this.endPage = pagecount;
            }

            logTrace("Setting ending page as [ " + this.endPage + " ]");
        } catch (IOException e) {
            throw new PDFValidationException("Failed to open PDF File", e);
        }
        logTrace("Exiting PDFUtil#updateStartAndEndPages");
    }
}
