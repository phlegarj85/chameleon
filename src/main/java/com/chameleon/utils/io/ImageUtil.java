package com.chameleon.utils.io;

import static com.chameleon.utils.TestReporter.logTrace;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import com.chameleon.utils.Constants;
import com.chameleon.utils.TestReporter;
import com.chameleon.utils.exception.ImageSaveException;

class ImageUtil {

    static boolean compareAndHighlight(final BufferedImage img1, final BufferedImage img2, String fileName, Color color) {
        logTrace("Entering ImageUtil#compareAndHighlight");
        final int w = img1.getWidth();
        final int h = img1.getHeight();
        final int[] p1 = img1.getRGB(0, 0, w, h, null, 0, w);
        final int[] p2 = img2.getRGB(0, 0, w, h, null, 0, w);

        logTrace("Compare images by pixels");
        if (!(java.util.Arrays.equals(p1, p2))) {
            logTrace("Images were not the same, begin to generate diff file image");
            logTrace("Change the color of pixel differences");
            for (int i = 0; i < p1.length; i++) {
                if (p1[i] != p2[i]) {
                    p1[i] = color.getRGB();
                }
            }

            logTrace("Generate diff image file");
            final BufferedImage out = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
            out.setRGB(0, 0, w, h, p1, 0, w);

            TestReporter.logFailure("Difference found in image. Difference highlighted in [ MAGENTA ] ");
            saveImage(out, fileName);
            logTrace("Entering ImageUtil#compareAndHighlight");
            return false;
        }
        logTrace("Exiting ImageUtil#compareAndHighlight");
        return true;
    }

    static void saveImage(BufferedImage image, String filename) {
        logTrace("Entering ImageUtil#saveImage");
        try {
            String slash = Constants.DIR_SEPARATOR;
            String destDir = Constants.SCREENSHOT_FOLDER + slash + filename.replace(".", slash);
            DateFormat dateFormat = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ssaa");
            String destFile = destDir + slash + filename + "_" + dateFormat.format(new Date()) + ".png";

            logTrace("Image to write in location [ " + destFile + " ]");

            // ensure direct exists first
            File outputfile = new File(destFile);
            outputfile.mkdirs();
            ImageIO.write(image, "png", outputfile);
            TestReporter.logScreenshot(outputfile, destFile);
        } catch (Exception e) {
            throw new ImageSaveException("Failed to write image to system", e);
        }
        logTrace("Exiting ImageUtil#saveImage");
    }
}