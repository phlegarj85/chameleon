package com.chameleon.api.soapServices.exceptions;

import com.chameleon.api.WebServiceException;

public class SoapException extends WebServiceException {
    private static final long serialVersionUID = -8710980695994382082L;

    public SoapException(String message, Throwable cause) {
        super("SOAP Error: " + message, cause);
    }

    public SoapException(String message, Object... args) {
        super("SOAP Error: " + message, args);
    }

    public SoapException(String message, Throwable cause, Object... args) {
        super("SOAP Error: " + message, cause, args);
    }
}
