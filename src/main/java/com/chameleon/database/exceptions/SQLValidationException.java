package com.chameleon.database.exceptions;

public class SQLValidationException extends RuntimeException {
    private static final long serialVersionUID = -2738970905210023091L;

    public SQLValidationException() {
        super();
    }

    public SQLValidationException(String message, Object... args) {
        super(String.format(message, args));
    }

    public SQLValidationException(String message, String sql, Object... args) {
        super(String.format(message, args) + ". SQL: " + sql);
    }

}