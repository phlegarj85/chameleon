package com.chameleon.database.databaseImpl;

import com.chameleon.database.Database;
import com.chameleon.database.DatabaseInfo;

/**
 * @deprecated
 *             Current process can lead to unnecessary configuration for the user.
 *             Instead use the {@link DatabaseInfo}
 * @author Justin Phlegar
 *
 */
@Deprecated
public class DB2Database extends Database {
    public DB2Database(String host, String port, String dbName) {
        super.driver = "COM.ibm.db2.jdbc.app.DB2Driver";
        super.connectionString = "jdbc:db2://" + host + ":" + port + "/" + dbName;
    }
}
