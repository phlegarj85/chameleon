package com.chameleon.database.databaseImpl;

import com.chameleon.database.Database;
import com.chameleon.database.DatabaseInfo;

/**
 * @deprecated
 *             Current process can lead to unnecessary configuration for the user.
 *             Instead use the {@link DatabaseInfo}
 * @author Justin Phlegar
 *
 */
@Deprecated
public class SQLServerDatabase extends Database {
    public SQLServerDatabase(String host, String port, String dbName) {
        super.driver = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
        super.connectionString = "jdbc:microsoft:sqlserver://" + host + ":" + port + ";DatabaseName=" + dbName;
    }
}
