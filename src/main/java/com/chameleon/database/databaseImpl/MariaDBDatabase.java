package com.chameleon.database.databaseImpl;

import com.chameleon.database.Database;
import com.chameleon.database.DatabaseInfo;

/**
 * @deprecated
 *             Current process can lead to unnecessary configuration for the user.
 *             Instead use the {@link DatabaseInfo}
 * @author Justin Phlegar
 *
 */
@Deprecated
public class MariaDBDatabase extends Database {
    public MariaDBDatabase(String host, String port, String dbName) {
        super.driver = "org.mariadb.jdbc.Driver";
        super.connectionString = "jdbc:mariadb://" + host + ":" + port + "/" + dbName;
    }
}
