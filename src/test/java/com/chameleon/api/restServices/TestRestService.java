package com.chameleon.api.restServices;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.api.APIBaseTest;
import com.chameleon.api.restServices.exceptions.RestException;
import com.chameleon.api.restServices.helpers.PostRequest;
import com.chameleon.utils.Sleeper;
import com.chameleon.utils.exception.JsonMapperException;
import com.chameleon.utils.io.JsonObjectMapper;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestRestService extends APIBaseTest {
    public final static String basePostsUrl = "https://jsonplaceholder.typicode.com/posts";

    @Feature("API")
    @Story("RestServices")

    @Test
    public void createRestService() {
        RestService rest = new RestService();
        Assert.assertNotNull(rest);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendGetRequestOnlyURL() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendGetRequest(basePostsUrl).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test(expectedExceptions = RestException.class)
    public void sendGetRequestInvalidURL() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendGetRequest("ht://jsplaceholder.typicode.com/posts").getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendGetRequestURLAndHeader() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendGetRequest(basePostsUrl, Headers.BASIC_CONVO()).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendGetRequestURLHeaderAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendGetRequest(basePostsUrl, Headers.JSON(), params).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPostRequestURLHeaderAndJson() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPostRequest(basePostsUrl, Headers.AUTH(), JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.CREATED);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPostRequestURLAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendPostRequest(basePostsUrl, params).getStatusCode() == ResponseCodes.CREATED);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPostRequestURLHeaderAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendPostRequest(basePostsUrl, Headers.JSON(), params).getStatusCode() == ResponseCodes.CREATED);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPostRequestURLHeaderJsonAndQueryParams() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPostRequest(basePostsUrl, Headers.BASIC_CONVO(), params, JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.CREATED);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPutRequestURLAndHeader() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPutRequest(basePostsUrl + "/1", Headers.AUTH()).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPutRequestURLHeaderAndJson() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPutRequest(basePostsUrl + "/1", Headers.AUTH(), JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPutRequestURLAndJson() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPutRequest(basePostsUrl + "/1", JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPutRequestURLAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendPutRequest(basePostsUrl + "/1", params).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPutRequestURLHeaderAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendPutRequest(basePostsUrl + "/1", Headers.JSON(), params).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPutRequestURLHeaderJsonAndQueryParams() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPutRequest(basePostsUrl + "/1", Headers.BASIC_CONVO(), params, JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPatchRequestURLParamsAndJson() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPatchRequest(basePostsUrl + "/1", params, JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPatchRequestURLHeaderAndJson() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPatchRequest(basePostsUrl + "/1", Headers.AUTH(), JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPatchRequestURLAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendPatchRequest(basePostsUrl + "/1", params).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPatchRequestURLHeaderAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendPatchRequest(basePostsUrl + "/1", Headers.JSON(), params).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendPatchRequestURLHeaderJsonAndQueryParams() {
        PostRequest request = new PostRequest();
        request.setUserId(1);
        request.setTitle("blah");
        request.setBody("blah");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));

        RestService rest = new RestService();
        Assert.assertTrue(rest.sendPatchRequest(basePostsUrl + "/1", Headers.BASIC_CONVO(), params, JsonObjectMapper.getJsonFromObject(request)).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendDeleteRequestURL() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendDeleteRequest(basePostsUrl + "/1").getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendDeleteRequestURLAndHeader() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendDeleteRequest(basePostsUrl + "/1", Headers.JSON()).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendDeleteRequestURLHeaderAndQueryParams() {
        RestService rest = new RestService();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("userId", "2"));
        Assert.assertTrue(rest.sendDeleteRequest(basePostsUrl + "/1", Headers.JSON(), params).getStatusCode() == ResponseCodes.OK);
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void sendOptionsRequestURL() {
        RestService rest = new RestService();
        Assert.assertTrue(rest.sendOptionsRequest(basePostsUrl + "/1").length > 0);
    }

    @Feature("API")
    @Story("RestServices")

    @Test(expectedExceptions = JsonMapperException.class)
    public void getJsonFromObjectExpectError() {
        JsonObjectMapper.getJsonFromObject(new Sleeper());
    }

    @Feature("API")
    @Story("RestServices")

    @Test
    public void addCustomHeaders() {
        RestService rest = new RestService();
        rest.addCustomHeaders("blah", "blahValue");
        RestResponse response = rest.sendGetRequest("http://google.com", Headers.BASIC_CONVO());
        Assert.assertTrue("blahValue".equals(response.getHttpRequest().getFirstHeader("blah").getValue()));
    }
}
