package com.chameleon.utils.dataHelpers;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.chameleon.AutomationException;
import com.chameleon.utils.dataHelpers.creditCards.CardType;
import com.chameleon.utils.dataHelpers.creditCards.CreditCard;
import com.chameleon.utils.dataHelpers.creditCards.CreditCardManager;
import com.chameleon.utils.dataHelpers.personFactory.Address;
import com.chameleon.utils.dataHelpers.personFactory.Person;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
public class TestCreditCard {
    private CreditCard card;
    private Person person = new Person();

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" })
    public void creditCardPersonConstructor() {
        card = CreditCardManager.getCreditCardByType(CardType.VISA, person);
        Assert.assertNotNull(card);
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" })
    public void creditCardPersonWillBillingConstructor() {
        Person peep = new Person();
        Address address = new Address();
        address.setType("Billing");
        peep.addAddress(address);
        CreditCard card = CreditCardManager.getCreditCardByType(CardType.VISA, peep);
        Assert.assertNotNull(card);
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getBillingCity() {
        Assert.assertTrue(card.getBillingCity().equals(person.primaryAddress().getCity()));
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getBillingCountry() {
        Assert.assertTrue(card.getBillingCountry().equals(person.primaryAddress().getCountryAbbv()));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getBillingState() {
        Assert.assertTrue(card.getBillingState().equals(person.primaryAddress().getStateAbbv()));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getBillingStreet() {
        Assert.assertTrue(card.getBillingStreet().equals(person.primaryAddress().getAddress1()));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getBillingStreet2() {
        Assert.assertTrue(card.getBillingStreet2().equals(person.primaryAddress().getAddress2()));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getBillingZip() {
        Assert.assertTrue(card.getBillingZip().equals(person.primaryAddress().getZipCode()));
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCardNumber() {
        Assert.assertTrue(card.getCardNumber().equals("4012888888881881"));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCardType() {
        Assert.assertTrue(card.getCardType().equals(CardType.VISA));
    }
    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getExpireMonth() {
        Assert.assertTrue(card.getExpireMonth().equals("12"));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getExpireYear() {
        Assert.assertTrue(card.getExpireYear().equals("20"));
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getNameOnCard() {
        Assert.assertTrue(card.getNameOnCard().equals(person.getFullName()));
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getSecurityCode() {
        Assert.assertTrue(card.getSecurityCode().equals("980"));
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_AMEX() {
        String type = "AMEX";
        CreditCard card = CreditCardManager.getCreditCardByType(type);
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.AMEX));
    }

    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_AMERICANEXPRESS() {
        CreditCard card = CreditCardManager.getCreditCardByType("American Express");
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.AMEX));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_DINERS() {
        CreditCard card = CreditCardManager.getCreditCardByType("DINERS");
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.DINERS_CLUB));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_DINERSCLUB() {
        String type = "DINERSCLUB";
        CreditCard card = CreditCardManager.getCreditCardByType(type);
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.DINERS_CLUB));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_DISC() {
        CreditCard card = CreditCardManager.getCreditCardByType("DISC");
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.DISCOVER));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_DISCOVER() {
        String type = "DISCOVER";
        CreditCard card = CreditCardManager.getCreditCardByType(type);
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.DISCOVER));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_JCB() {
        CreditCard card = CreditCardManager.getCreditCardByType("JCB");
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.JCB));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_MC() {
        CreditCard card = CreditCardManager.getCreditCardByType("MC");
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.MASTERCARD));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_MASTERCARD() {
        String type = "MASTERCARD";
        CreditCard card = CreditCardManager.getCreditCardByType(type);
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.MASTERCARD));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_VISA() {
        String type = "VISA";
        CreditCard card = CreditCardManager.getCreditCardByType(type);
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.VISA));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void getCreditCardByType_VISA_EXPIRED() {
        String type = "VISA_EXPIRED";
        CreditCard card = CreditCardManager.getCreditCardByType(type);
        Assert.assertNotNull(card);
        Assert.assertTrue(card.getCardType().equals(CardType.VISAEXPIRED));
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" }, expectedExceptions = AutomationException.class)
    public void getCreditCardByType_InvalidCard() {
        CreditCardManager.getCreditCardByType("BLAH");
    }


    @Feature("Utilities")
    @Story("CreditCard")
    
    @Test(groups = { "regression", "utils", "CreditCard" }, dependsOnMethods = { "creditCardPersonConstructor" })
    public void testToString() {
        Assert.assertTrue(card.toString().contains("[CreditCard cardType=VISA"));
    }
}
