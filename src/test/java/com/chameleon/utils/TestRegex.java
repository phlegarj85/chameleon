package com.chameleon.utils;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.web.WebBaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestRegex extends WebBaseTest {
    private static final String CASE_SENSITIVE_EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String CASE_INSENSITIVE_EMAIL_PATTERN = "^[_a-z0-9-\\+]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*(\\.[a-z]{2,})$";

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Feature("Utilities")
    @Story("Regex")

    @Test(groups = { "regression", "utils", "Regex" })
    public void constructorWithElement() {
        Assert.assertNotNull((new Regex()));
    }

    @Feature("Utilities")
    @Story("Regex")

    @Test(groups = { "regression", "utils", "dev", "Regex" })
    public void match() {
        Assert.assertTrue(Regex.match(CASE_SENSITIVE_EMAIL_PATTERN, "TesterEmail123@email.com"), "Pattern did not match");
    }

    @Feature("Utilities")
    @Story("Regex")

    @Test(groups = { "regression", "utils", "dev", "Regex" })
    public void matchFail() {
        Assert.assertFalse(Regex.match(CASE_SENSITIVE_EMAIL_PATTERN, "TesterEmail123email.com"), "Pattern did not match");
    }

    @Feature("Utilities")
    @Story("Regex")

    @Test(groups = { "regression", "utils", "dev", "Regex" })
    public void matchCaseInsensitive() {
        Assert.assertTrue(Regex.matchCaseInsensitive(CASE_INSENSITIVE_EMAIL_PATTERN, "TESTING234_23-234@email.com"), "Pattern did not match");
    }

    @Feature("Utilities")
    @Story("Regex")

    @Test(groups = { "regression", "utils", "dev", "Regex" })
    public void matchCaseInsensitiveFail() {
        Assert.assertFalse(Regex.matchCaseInsensitive(CASE_INSENSITIVE_EMAIL_PATTERN, "TesterEmail123@email.c"), "Pattern did not match");
    }
}
