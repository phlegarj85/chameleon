package com.chameleon.utils;

import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_BREAKDOWN_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_COMMON_FAILING_REASONS_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_EXECUTION_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_LONGEST_DURATION_SUMMARY_ENABLED;
import static com.chameleon.utils.cucumber.CucumberConstants.POM_CUCUMBER_OPTION_TEST_DETAILS_SUAMMRY_ENABLED;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.testng.annotations.Test;

import com.chameleon.BaseTest;
import com.chameleon.utils.cucumber.CucumberConstants;
import com.chameleon.utils.cucumber.CucumberMavenReport;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestCucumberMavenReport extends BaseTest {

    @Feature("Utilities")
    @Story("CucumberMavenReport")
    @Test(groups = { "regression", "utils", "CucumberMavenReport" })
    public void init() throws IOException {

        File directory = new File(Constants.CURRENT_DIR + CucumberConstants.RUNNER_RESULTS_LOCATION);
        directory.mkdirs();

        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + "/json/cucumber-parallel/*");
        for (Resource resource : resources) {
            File destination = new File(directory.getAbsolutePath() + Constants.DIR_SEPARATOR + resource.getFilename());
            FileUtils.copyURLToFile(resource.getURL(), destination);
        }

        System.setProperty(POM_CUCUMBER_OPTION_EXECUTION_SUMMARY_ENABLED, "true");
        System.setProperty(POM_CUCUMBER_OPTION_BREAKDOWN_SUMMARY_ENABLED, "true");
        System.setProperty(POM_CUCUMBER_OPTION_LONGEST_DURATION_SUMMARY_ENABLED, "true");
        System.setProperty(POM_CUCUMBER_OPTION_COMMON_FAILING_REASONS_SUMMARY_ENABLED, "true");
        System.setProperty(POM_CUCUMBER_OPTION_TEST_DETAILS_SUAMMRY_ENABLED, "true");
        CucumberMavenReport.main(null);

    }

}
