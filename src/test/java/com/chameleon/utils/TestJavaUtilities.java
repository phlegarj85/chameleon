package com.chameleon.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.web.WebBaseTest;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestJavaUtilities extends WebBaseTest {

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithNull() {
        Assert.assertFalse(JavaUtilities.isValid(null));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithValidObject() {
        Assert.assertTrue(JavaUtilities.isValid(1));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithValidString() {
        Assert.assertTrue(JavaUtilities.isValid("blah"));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithEmptyString() {
        Assert.assertFalse(JavaUtilities.isValid(""));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithValidCollection() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        Assert.assertTrue(JavaUtilities.isValid(list));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithEmptyCollection() {
        List<Integer> list = new ArrayList<>();
        Assert.assertFalse(JavaUtilities.isValid(list));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithValidMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put("blah", 1);
        Assert.assertTrue(JavaUtilities.isValid(map));
    }

    @Feature("Utilities")
    @Story("JavaUtilities")

    @Test(groups = { "regression", "utils", "javaUtilities" })
    public void testJavaUtilitiesWithEmptyMap() {
        Map<String, Integer> map = new HashMap<>();
        Assert.assertFalse(JavaUtilities.isValid(map));
    }

}
