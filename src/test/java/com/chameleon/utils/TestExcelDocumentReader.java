package com.chameleon.utils;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.chameleon.AutomationException;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.utils.io.ExcelDocumentReader;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestExcelDocumentReader extends WebBaseTest {

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataFilePathConstructorAllRows() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data");

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 3);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataFilePathConstructorSingleRow() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data", 1);

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 1);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataFilePathConstructorSheetNameSingleRow() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data", 1);

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 1);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataDefaultConstructorAllRows() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data");

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 3);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataDefaultConstructorAllRowsIncludeColumns() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data", -1, 0);

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 4);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("Column1"));
    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataDefaultConstructorAllRowsStartRow() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data", -1, 2);

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 2);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet2"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataDefaultConstructorSingleRow() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "Data", 1);

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 1);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataXlsFile() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData_xls.xls";
        data = ExcelDocumentReader.readData(filepath, "Data", 1);

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 1);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression", expectedExceptions = { AutomationException.class })
    public void readDataMissingXlsFile() {
        ExcelDocumentReader.readData("/excelsheets/TestData_blah.xls", "Data", 1);
    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression", expectedExceptions = { AutomationException.class })
    public void readDataInvalidXlsFile() {
        String filepath = "/excelsheets/TestData_csv.csv";
        ExcelDocumentReader.readData(filepath, "Data", 1);
    }

    @Feature("Utilities")
    @Story("ExcelDocumentReader")

    @Test(groups = "regression")
    public void readDataSheetAsIndex() {
        Object[][] data = null;
        String filepath = "/excelsheets/TestData.xlsx";
        data = ExcelDocumentReader.readData(filepath, "0");

        Assert.assertNotNull(data);
        Assert.assertTrue(data.length == 3);
        Assert.assertTrue(data[0].length == 5);
        Assert.assertTrue(data[0][0].toString().equals("DataSet1"));

    }
}
