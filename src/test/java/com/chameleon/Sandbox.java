package com.chameleon;

import org.testng.annotations.Test;

import com.chameleon.api.restServices.RestService;
import com.chameleon.utils.TestReporter;
import com.demo.DemoConstants;

public class Sandbox extends BaseTest {

    @Test
    public void test() {
        TestReporter.setDebugLevel(2);
        RestService rest = new RestService();
        rest.setPropertiesLocation(DemoConstants.REST_PROPERTIES);
        rest.sendGetRequest("/actors");

    }

}
