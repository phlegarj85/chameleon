package com.chameleon.selenium.web.by.angular;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.web.ExtendedWebDriver;
import com.chameleon.selenium.web.WebBaseTest;
import com.chameleon.selenium.web.elements.WebElement;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestAngular extends WebBaseTest {
    private ExtendedWebDriver driver;

    ByNG findByController = ByNG.controller("myCtrl");
    ByNG findByModel = ByNG.model("firstName");
    ByNG findByRepeater = ByNG.repeater("x in names | orderBy:'country'");
    ByNG findByShow = ByNG.show("myVar");

    @BeforeClass(groups = { "regression", "interfaces", "angular", "dev" })
    public void setup() {
        setApplicationUnderTest("Test Site");
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/angular/angularPage.html");
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" })
    public void driverFindNGController() {
        driver = testStart("TestAngular");
        WebElement controller = driver.findElement(findByController);
        Assert.assertTrue(controller.elementWired());
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController", expectedExceptions = IllegalArgumentException.class)
    public void driverFindNGControllerNullSearchBy() {
        driver.findElement(ByNG.controller(""));
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController")
    public void driverFindNGModel() {
        WebElement model = driver.findElement(findByModel);
        Assert.assertTrue(model.elementWired());

    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController", expectedExceptions = IllegalArgumentException.class)
    public void driverFindNGModelNullSearchBy() {
        driver.findElement(ByNG.model(""));
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController")
    public void driverFindNGRepeater() {
        WebElement repeater = driver.findElement(findByRepeater);
        Assert.assertTrue(repeater.elementWired());
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController", expectedExceptions = IllegalArgumentException.class)
    public void driverFindNGRepeaterNullSearchBy() {
        driver.findElement(ByNG.repeater(""));
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController")
    public void driverFindNGShow() {
        WebElement show = driver.findElement(findByShow);
        Assert.assertTrue(show.elementWired());
    }

    @Feature("Element Interfaces")
    @Story("Angular")

    @Test(groups = { "regression", "interfaces" }, dependsOnMethods = "driverFindNGController", expectedExceptions = IllegalArgumentException.class)
    public void driverFindNGShowNullSearchBy() {
        driver.findElement(ByNG.show(""));
    }

}
