package com.chameleon.selenium.web;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestWebBaseTest extends WebBaseTest {
    private String application = "application";
    private String browserUnderTest = "firefox";
    private String browserVersion = "57";
    private String operatingSystem = "Win10";
    private String runLocation = "local";
    private String testingEnvironment = "stage";
    private String testingName = "TestEnvironment";
    private String pageURL = "https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/core/interfaces/element.html";

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void testEnviroment() {
        WebBaseTest te = new WebBaseTest();
        Assert.assertNotNull(te);
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void applicationUnderTest() {
        WebBaseTest te = new WebBaseTest();
        te.setApplicationUnderTest(application);
        Assert.assertTrue(te.getApplicationUnderTest().equals(application));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void browserUnderTest() {
        WebBaseTest te = new WebBaseTest();
        te.setBrowserUnderTest(browserUnderTest);
        Assert.assertTrue(te.getBrowserUnderTest().equals(browserUnderTest));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void browserVersion() {
        WebBaseTest te = new WebBaseTest();
        te.setBrowserVersion(browserVersion);
        Assert.assertTrue(te.getBrowserVersion().equals(browserVersion));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void operatingSystem() {
        WebBaseTest te = new WebBaseTest();
        te.setOperatingSystem(operatingSystem);
        Assert.assertTrue(te.getOperatingSystem().equals(operatingSystem));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void runLocation() {
        setRunLocation(runLocation);
        Assert.assertTrue(getRunLocation().equals(runLocation));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void testEnvironment() {
        setEnvironment(testingEnvironment);
        Assert.assertTrue(getEnvironment().equals(testingEnvironment));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void testName() {
        setTestName(testingName);
        Assert.assertTrue(getTestName().equals(testingName));
    }

    @Feature("Utilities")
    @Story("TestEnvironment")

    @Test(groups = "regression")
    public void pageURL() {
        WebBaseTest te = new WebBaseTest();
        te.setPageURL(pageURL);
        Assert.assertTrue(te.getPageURL().equals(pageURL));
    }

}