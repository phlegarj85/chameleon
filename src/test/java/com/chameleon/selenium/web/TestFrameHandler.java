package com.chameleon.selenium.web;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.chameleon.selenium.DriverManager;
import com.chameleon.selenium.DriverType;
import com.chameleon.utils.Sleeper;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;

public class TestFrameHandler extends WebBaseTest {
    ExtendedWebDriver driver = null;

    @BeforeClass(groups = { "regression", "utils", "dev", "framehandler" })
    public void setup() {
        setPageURL("https://qtsemw.gitlab.io/chameleon-unit-test-site/sites/unitTests/chameleon/utils/frameHandler.html");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        DriverManager.setDriver(driver);
    }

    @Override
    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult testResults) {
    }

    @Override
    @AfterClass(alwaysRun = true)
    public void afterClass(ITestContext testResults) {
        DriverManager.setDriver(driver);
        endTest(getTestName(), testResults);
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @SuppressWarnings("deprecation")
    @Test(groups = { "regression", "utils", "dev", "framehandler" })
    public void findAndSwitchToFrameFromOutsideFrame() {
        driver = testStart("TestFrame");
        Sleeper.sleep(3);
        FrameHandler.findAndSwitchToFrame(driver, "menu_page");
        Assert.assertTrue(driver.findElement(By.id("googleLink")).isDisplayed(), "Link was not found in 'menu_page'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev",
            "framehandler" }, dependsOnMethods = "findAndSwitchToFrameFromOutsideFrame")
    public void testGetCurrentFrameName() {
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("menu_page"), "Frame name was not 'menu_page' ");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testGetCurrentFrameName")
    public void testGetDefaultContent() {
        FrameHandler.moveToDefaultContext(driver);
        Assert.assertNull(FrameHandler.getCurrentFrameName(driver), "Failed to move to default Content");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testGetDefaultContent")
    public void testMoveToChildFrameWithName() {
        FrameHandler.moveToChildFrame(driver, "main_page");
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_page"), "Button was not found in frame 'main_page'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testMoveToChildFrameWithName")
    public void testMoveToChildFrameWithLocator() {
        By locator = By.name("main_frame1");
        FrameHandler.moveToChildFrame(driver, locator);
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_frame1"), "Button was not found in frame 'main_frame1'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testMoveToChildFrameWithLocator")
    public void testMoveToParentFrame() {
        if (DriverType.SAFARI == driver.getDriverType()) {
            throw new SkipException("Test not valid for SafariDriver");
        }
        FrameHandler.moveToParentFrame(driver);
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_page"), "Button was not found in frame 'main_page'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testMoveToParentFrame")
    public void testMoveToSiblingFrameWithName() {
        if (DriverType.SAFARI == driver.getDriverType()) {
            throw new SkipException("Test not valid for SafariDriver");
        }
        FrameHandler.moveToChildFrame(driver, "main_frame1");
        FrameHandler.moveToSiblingFrame(driver, "main_frame2");
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_frame2"), "Button was not found in frame 'main_page'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testMoveToSiblingFrameWithName")
    public void testMoveToSiblingFrameWithLocator() {
        if (DriverType.SAFARI == driver.getDriverType()) {
            throw new SkipException("Test not valid for SafariDriver");
        }
        By locator = By.name("main_frame1");
        FrameHandler.moveToSiblingFrame(driver, locator);
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_frame1"), "Button was not found in frame 'main_frame1'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testMoveToSiblingFrameWithLocator")
    public void testMoveToMultiChildFrameWithName() {
        FrameHandler.moveToDefaultContext(driver);
        FrameHandler.moveToChildFrame(driver, new String[] { "main_page", "main_frame1" });
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_frame1"), "Button was not found in frame 'main_frame1'");
    }

    @Feature("Utilities")
    @Story("FrameHandler")

    @Test(groups = { "regression", "utils", "dev" }, dependsOnMethods = "testMoveToMultiChildFrameWithName")
    public void testMoveToMultiChildFrameWithLocator() {
        By locatorParentFrame = By.name("main_page");
        By locatorChildFrame = By.name("main_frame1");
        FrameHandler.moveToDefaultContext(driver);
        FrameHandler.moveToChildFrame(driver, new By[] { locatorParentFrame, locatorChildFrame });
        Assert.assertTrue(FrameHandler.getCurrentFrameName(driver).equals("main_frame1"), "Button was not found in frame 'main_frame1'");
    }

}
